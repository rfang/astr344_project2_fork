import numpy as np
import random
import matplotlib.pyplot as plt


# setting up the animation

plt.axis([0,100,-2,2])
plt.ion()
plt.show()
  
# define Car object
class Car:
	def __init__(self, Xposition = 0.0, Yposition = 0.0, velocity = 1.0, end = False):
		self.Xposition = Xposition
		self.Yposition = Yposition
		self.velocity = velocity
		self.end = end

	def move(self):
		self.Xposition = self.Xposition + self.velocity


cars = []
N = 50 # number of cars 
for i in range(N):
	cars.append(Car(Xposition = -i))

time = 0


while any(car.end == False for car in cars):
	# put speed back to 1
	for car in cars:
		car.velocity = 1.0

	# generate deer
	deer = random.randint(0,100)
	prob = random.random()

	if prob < 0.99: 
		j = 0
		while j < 5:
			for i in range(len(cars)):
				if (deer - cars[i].Xposition) >= 0 and (deer - cars[i].Xposition) <= 2:
					for k in range(i,len(cars)):
						cars[k].velocity *= 0.2


			for car in cars:
				car.move()
				if car.Xposition > 100:
					car.end = True

			time += 1

			Xpositions = np.array([car.Xposition for car in cars])
			Ypositions = np.array([car.Yposition for car in cars])
			plt.clf()
			plt.scatter(Xpositions,Ypositions, s=15, c='r', marker='.')
			plt.xlim(0,100)
			plt.ylim(-1,1)
			plt.draw()
			plt.pause(0.01)
			j += 1
			
	for car in cars:
		car.move()
		if car.Xposition > 100:
			car.end = True

	time += 1

	Xpositions = np.array([car.Xposition for car in cars])
	Ypositions = np.array([car.Yposition for car in cars])
	plt.clf()
	plt.scatter(Xpositions,Ypositions, s=15, c='r', marker='.')
	plt.xlim(0,100)
	plt.ylim(-1,1)
	plt.draw()
	plt.pause(0.01)
	


print time

